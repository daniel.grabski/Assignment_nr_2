/**
*	Author Daniel Grabski
*
* Assignment no. 2
* In a given sequence of integers entered from the console, check if there is an element whose sum of elements on its left is equal to the sum of elements on its right side. If there are no items on the left / right, set the sum to zero.
* Input:
* The first line specifies the number of test cases to check.
* In turn for each test case the first row determines the number of elements in a numerical sequence. On the other hand, the second line of a given test case contains a given number sequence.
* Output:
* For each test case, sign "Yes" on the console if there is an element referred to in the content of the task or "No" if it does not exist.
*/

#include <stdio.h>
#include <iostream>
#include <vector>
using namespace std;

/**
* Function get_int_value() check if enter data is intiger.
* @return ret 
*/

int get_int_value();

/**
* Function test() check if there is an element whose sum of elements on its left is equal to the sum of elements on its right side.
* @param nr_test test number
* @return result result test
*/

bool test(int nr_test);

/**
* Function load_elements() load number to test and check number of elements
* @param element vector for data storage
* @param number_of_element number of correct elements
* @return result result test
*/

void load_elements(vector<int> &element, int number_of_element);

int main()
{

	int number_of_tests;
	cout << "enter the number of tests:" << endl;
	number_of_tests = get_int_value();
	bool *result_of_tests = new bool[number_of_tests];	
	
	for (int i = 0; i < number_of_tests; i++)
	{	
		result_of_tests[i] = test(i);
	}


	for (int i = 0; i < number_of_tests; i++)
	{
		if(result_of_tests[i]==true)
		{
			cout << "Yes"<<endl;
		}
		else
		{
			cout << "No" << endl;
		}
	}
	delete result_of_tests;

	system("pause");
	return 0;
}

int get_int_value()
{
	int ret;

	if (cin >> ret)
	{
		return ret;
	}
	else
	{
		while (!(cin >> ret))
		{
			cout << "wrong value enter number again" << endl;
			cin.clear();
			while (cin.get() != '\n')
				continue;
		}
		return  ret;
	}


}	
bool test(int nr_test)
{
	bool result = false;
	int number_of_elements;
	int total_sum = 0;
	int sum = 0;
	cout << "enter the number of elemets in test " << nr_test + 1 << ":" << endl;
	number_of_elements = get_int_value();
	vector<int> element;
	load_elements(element, number_of_elements);
	
	for (int j = 0; j < number_of_elements; j++)
	{
		total_sum += element[j];
	}
	for (int j = 0; j < number_of_elements; j++)
	{
		total_sum -= element[j];
		if (sum == total_sum)
		{
			result = true;
			break;
		}
		sum += element[j];

	}

	return result;
}
void load_elements(vector<int> &element, int number_of_element)
{
	cout << "enter the numbers" << endl;

	for (int i = 0; i < number_of_element; i++)
	{
		element.push_back(get_int_value());
		if (getchar() == '\n')
		{
			cin.clear();
			break;
		}
	}

	if (element.size() != number_of_element)
	{
		cin.clear();
		cout << "wrong number of elements enter numbers again" << endl;
		element.clear();
		load_elements(element, number_of_element);
		
	}

}